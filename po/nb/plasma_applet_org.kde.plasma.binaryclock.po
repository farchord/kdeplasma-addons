# Translation of plasma_applet_org.kde.plasma.binaryclock to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-17 01:38+0000\n"
"PO-Revision-Date: 2009-05-24 21:15+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 0.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/config/config.qml:17
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/configGeneral.qml:33
#, kde-format
msgid "Display:"
msgstr ""

#: package/contents/ui/configGeneral.qml:34
#: package/contents/ui/configGeneral.qml:88
#, kde-format
msgctxt "@option:check"
msgid "Grid"
msgstr ""

#: package/contents/ui/configGeneral.qml:39
#: package/contents/ui/configGeneral.qml:75
#, kde-format
msgctxt "@option:check"
msgid "Inactive LEDs"
msgstr ""

#: package/contents/ui/configGeneral.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Seconds"
msgstr ""

#: package/contents/ui/configGeneral.qml:49
#, kde-format
msgctxt "@option:check"
msgid "In BCD format (decimal)"
msgstr ""

#: package/contents/ui/configGeneral.qml:58
#, kde-format
msgid "Use custom color for:"
msgstr ""

#: package/contents/ui/configGeneral.qml:62
#, kde-format
msgctxt "@option:check"
msgid "Active LEDs"
msgstr ""
