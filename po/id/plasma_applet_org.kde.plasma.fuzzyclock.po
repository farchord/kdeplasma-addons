# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Wantoyo <wantoyek@gmail.com>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-21 02:02+0000\n"
"PO-Revision-Date: 2019-08-11 13:36+0700\n"
"Last-Translator: Wantoyo <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Penampilan"

#: package/contents/ui/configAppearance.qml:24
#, kde-format
msgctxt "@title:group"
msgid "Font:"
msgstr "Font:"

#: package/contents/ui/configAppearance.qml:25
#, kde-format
msgctxt "@option:check"
msgid "Bold text"
msgstr "Teks tebal"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgctxt "@option:check"
msgid "Italic text"
msgstr "Teks miring"

#: package/contents/ui/configAppearance.qml:39
#, kde-format
msgctxt "@title:group"
msgid "Fuzzyness:"
msgstr "Kekurangtepatan:"

#: package/contents/ui/configAppearance.qml:49
#, kde-format
msgctxt "@item:inrange"
msgid "Accurate"
msgstr "Tepat"

#: package/contents/ui/configAppearance.qml:57
#, kde-format
msgctxt "@item:inrange"
msgid "Fuzzy"
msgstr "Kurang Tepat"

#: package/contents/ui/FuzzyClock.qml:35
#, kde-format
msgid "One o’clock"
msgstr "Pukul satu"

#: package/contents/ui/FuzzyClock.qml:36
#, kde-format
msgid "Five past one"
msgstr "Satu lebih lima"

#: package/contents/ui/FuzzyClock.qml:37
#, kde-format
msgid "Ten past one"
msgstr "Satu lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:38
#, kde-format
msgid "Quarter past one"
msgstr "Satu lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:39
#, kde-format
msgid "Twenty past one"
msgstr "Satu lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:40
#, kde-format
msgid "Twenty-five past one"
msgstr "Satu lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:41
#, kde-format
msgid "Half past one"
msgstr "Setengah dua"

#: package/contents/ui/FuzzyClock.qml:42
#, kde-format
msgid "Twenty-five to two"
msgstr "Dua kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:43
#, kde-format
msgid "Twenty to two"
msgstr "Dua kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:44
#, kde-format
msgid "Quarter to two"
msgstr "Dua kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:45
#, kde-format
msgid "Ten to two"
msgstr "Dua kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:46
#, kde-format
msgid "Five to two"
msgstr "Dua kurang lima"

#: package/contents/ui/FuzzyClock.qml:47
#, kde-format
msgid "Two o’clock"
msgstr "Pukul dua"

#: package/contents/ui/FuzzyClock.qml:48
#, kde-format
msgid "Five past two"
msgstr "Dua lebih lima"

#: package/contents/ui/FuzzyClock.qml:49
#, kde-format
msgid "Ten past two"
msgstr "Dua lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:50
#, kde-format
msgid "Quarter past two"
msgstr "Dua lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:51
#, kde-format
msgid "Twenty past two"
msgstr "Dua lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:52
#, kde-format
msgid "Twenty-five past two"
msgstr "Dua lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:53
#, kde-format
msgid "Half past two"
msgstr "Setengah tiga"

#: package/contents/ui/FuzzyClock.qml:54
#, kde-format
msgid "Twenty-five to three"
msgstr "Tiga kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:55
#, kde-format
msgid "Twenty to three"
msgstr "Tiga kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:56
#, kde-format
msgid "Quarter to three"
msgstr "Tiga kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:57
#, kde-format
msgid "Ten to three"
msgstr "Tiga kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:58
#, kde-format
msgid "Five to three"
msgstr "Tiga kurang lima"

#: package/contents/ui/FuzzyClock.qml:59
#, kde-format
msgid "Three o’clock"
msgstr "Pukul tiga"

#: package/contents/ui/FuzzyClock.qml:60
#, kde-format
msgid "Five past three"
msgstr "Tiga lebih lima"

#: package/contents/ui/FuzzyClock.qml:61
#, kde-format
msgid "Ten past three"
msgstr "Tiga lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:62
#, kde-format
msgid "Quarter past three"
msgstr "Tiga lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:63
#, kde-format
msgid "Twenty past three"
msgstr "Tiga lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:64
#, kde-format
msgid "Twenty-five past three"
msgstr "Tiga lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:65
#, kde-format
msgid "Half past three"
msgstr "Setengah empat"

#: package/contents/ui/FuzzyClock.qml:66
#, kde-format
msgid "Twenty-five to four"
msgstr "Empat kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:67
#, kde-format
msgid "Twenty to four"
msgstr "Empat kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:68
#, kde-format
msgid "Quarter to four"
msgstr "Empat kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:69
#, kde-format
msgid "Ten to four"
msgstr "Empat kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:70
#, kde-format
msgid "Five to four"
msgstr "Empat kurang lima"

#: package/contents/ui/FuzzyClock.qml:71
#, kde-format
msgid "Four o’clock"
msgstr "Pukul empat"

#: package/contents/ui/FuzzyClock.qml:72
#, kde-format
msgid "Five past four"
msgstr "Empat lebih lima"

#: package/contents/ui/FuzzyClock.qml:73
#, kde-format
msgid "Ten past four"
msgstr "Empat lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:74
#, kde-format
msgid "Quarter past four"
msgstr "Empat lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:75
#, kde-format
msgid "Twenty past four"
msgstr "Empat lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:76
#, kde-format
msgid "Twenty-five past four"
msgstr "Empat lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:77
#, kde-format
msgid "Half past four"
msgstr "Setengah lima"

#: package/contents/ui/FuzzyClock.qml:78
#, kde-format
msgid "Twenty-five to five"
msgstr "Lima kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:79
#, kde-format
msgid "Twenty to five"
msgstr "Lima kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:80
#, kde-format
msgid "Quarter to five"
msgstr "Lima kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:81
#, kde-format
msgid "Ten to five"
msgstr "Lima kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:82
#, kde-format
msgid "Five to five"
msgstr "Lima kurang lima"

#: package/contents/ui/FuzzyClock.qml:83
#, kde-format
msgid "Five o’clock"
msgstr "Pukul lima"

#: package/contents/ui/FuzzyClock.qml:84
#, kde-format
msgid "Five past five"
msgstr "Lima lebih lima"

#: package/contents/ui/FuzzyClock.qml:85
#, kde-format
msgid "Ten past five"
msgstr "Lima lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:86
#, kde-format
msgid "Quarter past five"
msgstr "Lima lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:87
#, kde-format
msgid "Twenty past five"
msgstr "Lima lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:88
#, kde-format
msgid "Twenty-five past five"
msgstr "Lima lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:89
#, kde-format
msgid "Half past five"
msgstr "Setengah enam"

#: package/contents/ui/FuzzyClock.qml:90
#, kde-format
msgid "Twenty-five to six"
msgstr "Enam kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:91
#, kde-format
msgid "Twenty to six"
msgstr "Enam kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:92
#, kde-format
msgid "Quarter to six"
msgstr "Enam kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:93
#, kde-format
msgid "Ten to six"
msgstr "Enam kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:94
#, kde-format
msgid "Five to six"
msgstr "Enam kurang lima"

#: package/contents/ui/FuzzyClock.qml:95
#, kde-format
msgid "Six o’clock"
msgstr "Pukul enam"

#: package/contents/ui/FuzzyClock.qml:96
#, kde-format
msgid "Five past six"
msgstr "Enam lebih lima"

#: package/contents/ui/FuzzyClock.qml:97
#, kde-format
msgid "Ten past six"
msgstr "Enam lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:98
#, kde-format
msgid "Quarter past six"
msgstr "Enam lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:99
#, kde-format
msgid "Twenty past six"
msgstr "Enam lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:100
#, kde-format
msgid "Twenty-five past six"
msgstr "Enam lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:101
#, kde-format
msgid "Half past six"
msgstr "Setengah tujuh"

#: package/contents/ui/FuzzyClock.qml:102
#, kde-format
msgid "Twenty-five to seven"
msgstr "Tujuh kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:103
#, kde-format
msgid "Twenty to seven"
msgstr "Tujuh kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:104
#, kde-format
msgid "Quarter to seven"
msgstr "Tujuh kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:105
#, kde-format
msgid "Ten to seven"
msgstr "Tujuh kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:106
#, kde-format
msgid "Five to seven"
msgstr "Tujuh kurang lima"

#: package/contents/ui/FuzzyClock.qml:107
#, kde-format
msgid "Seven o’clock"
msgstr "Pukul tujuh"

#: package/contents/ui/FuzzyClock.qml:108
#, kde-format
msgid "Five past seven"
msgstr "Tujuh lebih lima"

#: package/contents/ui/FuzzyClock.qml:109
#, kde-format
msgid "Ten past seven"
msgstr "Tujuh lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:110
#, kde-format
msgid "Quarter past seven"
msgstr "Tujuh lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:111
#, kde-format
msgid "Twenty past seven"
msgstr "Tujuh lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:112
#, kde-format
msgid "Twenty-five past seven"
msgstr "Tujuh lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:113
#, kde-format
msgid "Half past seven"
msgstr "Setengah delapan"

#: package/contents/ui/FuzzyClock.qml:114
#, kde-format
msgid "Twenty-five to eight"
msgstr "Delapan kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:115
#, kde-format
msgid "Twenty to eight"
msgstr "Delapan kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:116
#, kde-format
msgid "Quarter to eight"
msgstr "Delapan kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:117
#, kde-format
msgid "Ten to eight"
msgstr "Delapan kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:118
#, kde-format
msgid "Five to eight"
msgstr "Delapan kurang lima"

#: package/contents/ui/FuzzyClock.qml:119
#, kde-format
msgid "Eight o’clock"
msgstr "Pukul delapan"

#: package/contents/ui/FuzzyClock.qml:120
#, kde-format
msgid "Five past eight"
msgstr "Delapan lebih lima"

#: package/contents/ui/FuzzyClock.qml:121
#, kde-format
msgid "Ten past eight"
msgstr "Delapan lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:122
#, kde-format
msgid "Quarter past eight"
msgstr "Delapan lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:123
#, kde-format
msgid "Twenty past eight"
msgstr "Delapan lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:124
#, kde-format
msgid "Twenty-five past eight"
msgstr "Delapan lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:125
#, kde-format
msgid "Half past eight"
msgstr "Setengah sembilan"

#: package/contents/ui/FuzzyClock.qml:126
#, kde-format
msgid "Twenty-five to nine"
msgstr "Sembilan kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:127
#, kde-format
msgid "Twenty to nine"
msgstr "Sembilan kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:128
#, kde-format
msgid "Quarter to nine"
msgstr "Sembilan kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:129
#, kde-format
msgid "Ten to nine"
msgstr "Sembilan kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:130
#, kde-format
msgid "Five to nine"
msgstr "Sembilan kurang lima"

#: package/contents/ui/FuzzyClock.qml:131
#, kde-format
msgid "Nine o’clock"
msgstr "Pukul sembilan"

#: package/contents/ui/FuzzyClock.qml:132
#, kde-format
msgid "Five past nine"
msgstr "Sembilan lebih lima"

#: package/contents/ui/FuzzyClock.qml:133
#, kde-format
msgid "Ten past nine"
msgstr "Sembilan lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:134
#, kde-format
msgid "Quarter past nine"
msgstr "Sembilan lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:135
#, kde-format
msgid "Twenty past nine"
msgstr "Sembilan lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:136
#, kde-format
msgid "Twenty-five past nine"
msgstr "Sembilan lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:137
#, kde-format
msgid "Half past nine"
msgstr "Setengah sepuluh"

#: package/contents/ui/FuzzyClock.qml:138
#, kde-format
msgid "Twenty-five to ten"
msgstr "Sepuluh kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:139
#, kde-format
msgid "Twenty to ten"
msgstr "Sepuluh kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:140
#, kde-format
msgid "Quarter to ten"
msgstr "Sepuluh kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:141
#, kde-format
msgid "Ten to ten"
msgstr "Sepuluh kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:142
#, kde-format
msgid "Five to ten"
msgstr "Sepuluh kurang lima"

#: package/contents/ui/FuzzyClock.qml:143
#, kde-format
msgid "Ten o’clock"
msgstr "Pukul sepuluh"

#: package/contents/ui/FuzzyClock.qml:144
#, kde-format
msgid "Five past ten"
msgstr "Sepuluh lebih lima"

#: package/contents/ui/FuzzyClock.qml:145
#, kde-format
msgid "Ten past ten"
msgstr "Sepuluh lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:146
#, kde-format
msgid "Quarter past ten"
msgstr "Sepuluh lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:147
#, kde-format
msgid "Twenty past ten"
msgstr "Sepuluh lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:148
#, kde-format
msgid "Twenty-five past ten"
msgstr "Sepuluh lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:149
#, kde-format
msgid "Half past ten"
msgstr "Setengah sebelas"

#: package/contents/ui/FuzzyClock.qml:150
#, kde-format
msgid "Twenty-five to eleven"
msgstr "Sebelas kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:151
#, kde-format
msgid "Twenty to eleven"
msgstr "Sebelas kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:152
#, kde-format
msgid "Quarter to eleven"
msgstr "Sebelas kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:153
#, kde-format
msgid "Ten to eleven"
msgstr "Sebelas kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:154
#, kde-format
msgid "Five to eleven"
msgstr "Sebelas kurang lima"

#: package/contents/ui/FuzzyClock.qml:155
#, kde-format
msgid "Eleven o’clock"
msgstr "Pukul sebelas"

#: package/contents/ui/FuzzyClock.qml:156
#, kde-format
msgid "Five past eleven"
msgstr "Sebelas lebih lima"

#: package/contents/ui/FuzzyClock.qml:157
#, kde-format
msgid "Ten past eleven"
msgstr "Sebelas lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:158
#, kde-format
msgid "Quarter past eleven"
msgstr "Sebelas lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:159
#, kde-format
msgid "Twenty past eleven"
msgstr "Sebelas lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:160
#, kde-format
msgid "Twenty-five past eleven"
msgstr "Sebelas lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:161
#, kde-format
msgid "Half past eleven"
msgstr "Setengah dua belas"

#: package/contents/ui/FuzzyClock.qml:162
#, kde-format
msgid "Twenty-five to twelve"
msgstr "Dua belas kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:163
#, kde-format
msgid "Twenty to twelve"
msgstr "Dua belas kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:164
#, kde-format
msgid "Quarter to twelve"
msgstr "Dua belas kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:165
#, kde-format
msgid "Ten to twelve"
msgstr "Dua belas kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:166
#, kde-format
msgid "Five to twelve"
msgstr "Dua belas kurang lima"

#: package/contents/ui/FuzzyClock.qml:167
#, kde-format
msgid "Twelve o’clock"
msgstr "Pukul dua belas"

#: package/contents/ui/FuzzyClock.qml:168
#, kde-format
msgid "Five past twelve"
msgstr "Dua belas lebih lima"

#: package/contents/ui/FuzzyClock.qml:169
#, kde-format
msgid "Ten past twelve"
msgstr "Dua belas lebih sepuluh"

#: package/contents/ui/FuzzyClock.qml:170
#, kde-format
msgid "Quarter past twelve"
msgstr "Dua belas lebih seperempat"

#: package/contents/ui/FuzzyClock.qml:171
#, kde-format
msgid "Twenty past twelve"
msgstr "Dua belas lebih dua puluh"

#: package/contents/ui/FuzzyClock.qml:172
#, kde-format
msgid "Twenty-five past twelve"
msgstr "Dua belas lebih dua puluh lima"

# Orang sering menyebutkan ini
#: package/contents/ui/FuzzyClock.qml:173
#, kde-format
msgid "Half past twelve"
msgstr "Setengah satu"

#: package/contents/ui/FuzzyClock.qml:174
#, kde-format
msgid "Twenty-five to one"
msgstr "Satu kurang dua puluh lima"

#: package/contents/ui/FuzzyClock.qml:175
#, kde-format
msgid "Twenty to one"
msgstr "Satu kurang dua puluh"

#: package/contents/ui/FuzzyClock.qml:176
#, kde-format
msgid "Quarter to one"
msgstr "Satu kurang seperempat"

#: package/contents/ui/FuzzyClock.qml:177
#, kde-format
msgid "Ten to one"
msgstr "Satu kurang sepuluh"

#: package/contents/ui/FuzzyClock.qml:178
#, kde-format
msgid "Five to one"
msgstr "Satu kurang lima"

#: package/contents/ui/FuzzyClock.qml:182
#, kde-format
msgid "Sleep"
msgstr "Tidur"

#: package/contents/ui/FuzzyClock.qml:182
#, kde-format
msgid "Breakfast"
msgstr "Sarapan"

#: package/contents/ui/FuzzyClock.qml:182
#, kde-format
msgid "Second Breakfast"
msgstr "Sarapan Kedua"

# In Indonesian there are no that
#: package/contents/ui/FuzzyClock.qml:182
#, kde-format
msgid "Elevenses"
msgstr "Makan ringan siang"

#: package/contents/ui/FuzzyClock.qml:183
#, kde-format
msgid "Lunch"
msgstr "Makan siang"

#: package/contents/ui/FuzzyClock.qml:183
#, kde-format
msgid "Afternoon tea"
msgstr "Teh sore"

#: package/contents/ui/FuzzyClock.qml:183
#, kde-format
msgid "Dinner"
msgstr "Makan malam"

#: package/contents/ui/FuzzyClock.qml:183
#, kde-format
msgid "Supper"
msgstr "Makan malam"

#: package/contents/ui/FuzzyClock.qml:187
#, kde-format
msgid "Night"
msgstr "Malam"

#: package/contents/ui/FuzzyClock.qml:187
#, kde-format
msgid "Early morning"
msgstr "Dini hari"

#: package/contents/ui/FuzzyClock.qml:187
#, kde-format
msgid "Morning"
msgstr "Pagi"

#: package/contents/ui/FuzzyClock.qml:187
#, kde-format
msgid "Almost noon"
msgstr "Agak siang"

#: package/contents/ui/FuzzyClock.qml:188
#, kde-format
msgid "Noon"
msgstr "Siang"

#: package/contents/ui/FuzzyClock.qml:188
#, kde-format
msgid "Afternoon"
msgstr "Sore"

#: package/contents/ui/FuzzyClock.qml:188
#, kde-format
msgid "Evening"
msgstr "Malam"

#: package/contents/ui/FuzzyClock.qml:188
#, kde-format
msgid "Late evening"
msgstr "Larut malam"

#: package/contents/ui/FuzzyClock.qml:192
#, kde-format
msgid "Start of week"
msgstr "Awal pekan"

#: package/contents/ui/FuzzyClock.qml:192
#, kde-format
msgid "Middle of week"
msgstr "Pertengahan pekan"

#: package/contents/ui/FuzzyClock.qml:192
#, kde-format
msgid "End of week"
msgstr "Akhir pekan"

#: package/contents/ui/FuzzyClock.qml:192
#, kde-format
msgid "Weekend!"
msgstr "Akhir pekan!"
