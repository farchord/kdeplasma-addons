# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# giovanni <g.sora@tiscali.it>, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-17 01:38+0000\n"
"PO-Revision-Date: 2023-06-28 11:15+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Claves"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Monstra quando activate:"

#: contents/ui/configAppearance.qml:40
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Caps Lock"

#: contents/ui/configAppearance.qml:47
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num Lock"

#: contents/ui/main.qml:42
#, kde-format
msgid "Caps Lock activated"
msgstr "Caps Lock activate"

#: contents/ui/main.qml:43
#, kde-format
msgid "Num Lock activated"
msgstr "Num Lock activate"

#: contents/ui/main.qml:114
#, kde-format
msgid "No lock keys activated"
msgstr "Necun claves de bloco (num lock) activate"

#~ msgid "Num Lock"
#~ msgstr "Num Lock"

#~ msgid "%1: Locked\n"
#~ msgstr "%1: Blocate\n"

#~ msgid "Unlocked"
#~ msgstr "Disblocate"
